
**Do not send a pull request to this GitHub repository**.

For more detail, please see [official website] wiki [Contribute].

[official website]: http://www.redmine.org
[Contribute]: http://www.redmine.org/projects/redmine/wiki/Contribute


# plugins

https://www.redmineup.com/pages/plugins/agile#features
https://github.com/masweetman/issue_charts
